let mapleader=" "
set nocompatible
set mouse=a
filetype on
filetype plugin on
let g:mkdp_auto_start=0

set encoding=utf-8
let mapleader=" "
syntax on
autocmd BufWritePost $MYVIMRC source $MYVIMRC

colorscheme industry
map ss :shell<CR>
set background=dark
set mouse=a
set showmode
set showcmd
set autoindent
"set smartindent
set cindent
set shiftwidth=4
set tabstop=4
set softtabstop=4
set showmatch
imap jj <Esc>
"map cc :!ctags -R .<CR>

autocmd FileType c inoremap ;; <Esc>A;<Esc>
autocmd FileType c inoremap ( ()<Esc>i
autocmd FileType c inoremap [ []<Esc>i
autocmd FileType c inoremap { {}<Esc>i
autocmd FileType c inoremap " ""<Esc>i
autocmd FileType c inoremap ' ''<Esc>i
autocmd FileType c inoremap .. ->
autocmd FileType c inoremap ,, <Esc>f"a, 
autocmd FileType python set tabstop=4 | set expandtad | set autoindent | set textwidth=79j
autocmd FileType py inoremap ' ''<Esc>i
autocmd FileType py inoremap " ""<Esc>i
autocmd FileType py inoremap ( ()<Esc>i
autocmd FileType py inoremap { {}<Esc>i
autocmd FileType sh inoremap { {}<Esc>i
autocmd FileType sh inoremap ( ()<Esc>i
autocmd FileType sh inoremap [ []<Esc>i
autocmd FileType sh inoremap " ""<Esc>i

nnoremap <silent><F8> :TlistToggle<CR>
let Tlist_Show_One_File=1    "只显示当前文件的tags
let Tlist_WinWidth=40        "设置taglist宽度
let Tlist_Exit_OnlyWindow=1  "tagList窗口是最后一个窗口，则退出Vim
let Tlist_Use_Right_Window=1 "在Vim窗口右侧显示taglist窗口

set nu
set ruler
set wrap
set wildmenu
set t_Co=256

set hlsearch
set incsearch
noremap <LEADER><CR> :nohlsearch<CR>
noremap <LEADER>t :term bash<CR>
map <leader>f gd

map J 5j
map K 5k

nmap <leader>w :w!<cr>
noremap n nzz
noremap N Nzz
map <leader><leader> ggdG
" 分屏，改变分屏size，tab
map sl :set splitright<CR>:vsplit<CR>
map sh :set nosplitright<CR>:vsplit<CR>
map si :set nosplitbelow<CR>:split<CR>
map sk :set splitbelow<CR>:split<CR>
map <up> :res +5<CR>
map <down> :res -5<CR>
map <left> :vertical resize -5<CR>
map <right> :vertical resize +5<CR>
map tu :tabe<CR>

" 重新打开文件，光标放到关闭前的位置
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

